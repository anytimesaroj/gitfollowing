package dialogutils.blooms.com.testdunjo;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import java.util.List;

import dialogutils.blooms.com.testdunjo.model.Item;
import dialogutils.blooms.com.testdunjo.service.TransportManager;

public class MainActivityPresenter implements MainActivityContract.Presenter {

    private MainActivityContract.View view;
    Activity mContext;

    public MainActivityPresenter(MainActivityContract.View view) {
        this.view = view;
        initPresenter();
    }

    @Override
    public void getSearchText(String search) {
        if (TextUtils.isEmpty(search)) {
            view.displayErrorMessage("Text Field is Empty");
        } else {
            view.displayProgress();
            TransportManager.getInstance().searchUser(search);
        }
    }

    @Override
    public void getListFollowers(List<Item> searchList) {
        if (searchList != null && searchList.size() > 0) {
            view.displayFollowers(searchList);
        } else {
            view.displayErrorMessage("No record Found");
        }
        view.hideProgress();
    }

    private void initPresenter() {
        view.initViews();
    }


    @Override
    public void setView(View view) {

    }

    @Override
    public void setActivity(Activity activity) {

    }
}