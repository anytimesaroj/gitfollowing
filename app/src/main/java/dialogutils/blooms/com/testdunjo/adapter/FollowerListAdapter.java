package dialogutils.blooms.com.testdunjo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dialogutils.blooms.com.testdunjo.R;
import dialogutils.blooms.com.testdunjo.model.Item;

public class FollowerListAdapter extends RecyclerView.Adapter<FollowerListAdapter.ViewHolder> {

    Context context;
    List<Item> items;

    public FollowerListAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    public void updateList(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public FollowerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.follower_row_item, parent, false);
        FollowerListAdapter.ViewHolder VH = new FollowerListAdapter.ViewHolder(row);
        return VH;
    }

    @Override
    public void onBindViewHolder(final FollowerListAdapter.ViewHolder holder, final int position) {
//        holder.img = items.get(position).getAvatarUrl();
        Item item = items.get(position);
        holder.name.setText(item.getLogin());
        Picasso.with(context)
                .load(item.getAvatarUrl())
                .into(holder.img);
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        ImageView img;

        private View mView;

        ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            img = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
        }
    }
}

