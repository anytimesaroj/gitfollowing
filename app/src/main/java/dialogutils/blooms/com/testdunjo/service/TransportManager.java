package dialogutils.blooms.com.testdunjo.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import dialogutils.blooms.com.testdunjo.model.Item;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TransportManager {
    static TransportManager manager;
    ConnectionListener listener;

    public static TransportManager getInstance() {
        if (manager == null)
            manager = new TransportManager();
        return manager;
    }

    public void setListener(ConnectionListener listener) {
        this.listener = listener;
    }

    IApiService iApiService;

    public IApiService getService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

//        Log.d(TAG, "getUPIService: ");
        if (iApiService == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            iApiService = retrofit.create(IApiService.class);
        }
        return iApiService;
    }


    public void searchUser(String user) {
        getService().search(user).enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                listener.onSuccessResponse(IApiService.searchUser, response.body());
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                listener.onFailureResponse(IApiService.searchUser, t.getLocalizedMessage());
            }
        });
    }


    public interface ConnectionListener {

        void onSuccessResponse(int reqType, Object data);

        void onFailureResponse(int reqType, Object data);
    }
}
