package dialogutils.blooms.com.testdunjo.base;

import android.app.Activity;
import android.view.View;

public interface BasePresenter {
    void setView(View view);
    void setActivity(Activity activity);
}
