package dialogutils.blooms.com.testdunjo.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

    void initViews();

    void successResult();

    void failureResult();
}
