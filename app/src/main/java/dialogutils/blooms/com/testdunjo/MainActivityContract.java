package dialogutils.blooms.com.testdunjo;

import java.util.List;

import dialogutils.blooms.com.testdunjo.base.BasePresenter;
import dialogutils.blooms.com.testdunjo.base.BaseView;
import dialogutils.blooms.com.testdunjo.model.Item;

public interface MainActivityContract {

    interface View extends BaseView<Presenter> {
        void displayFollowers(List<Item> follow);

        void displayProgress();

        void hideProgress();

        void displayErrorMessage(String message);
    }

    interface Presenter extends BasePresenter {
        void getSearchText(String search);

        void getListFollowers(List<Item> follow);

    }
}
