package dialogutils.blooms.com.testdunjo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import dialogutils.blooms.com.testdunjo.DialogUtils.Dialog;
import dialogutils.blooms.com.testdunjo.adapter.FollowerListAdapter;
import dialogutils.blooms.com.testdunjo.model.Item;
import dialogutils.blooms.com.testdunjo.service.TransportManager;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View, View.OnClickListener, TransportManager.ConnectionListener {

  private MainActivityContract.Presenter presenter;
  RecyclerView followerList;
  EditText searchEdit;
  Button searchBtn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    presenter = new MainActivityPresenter(this);

  }

  FollowerListAdapter adapter;

  @Override
  public void displayFollowers(List<Item> follow) {
    if (adapter == null)
      adapter = new FollowerListAdapter(getApplicationContext(), follow);
    else
      adapter.updateList(follow);
    followerList.setAdapter(adapter);
  }

  @Override
  public void displayProgress() {
    Dialog.showProgressBar(this);
  }

  @Override
  public void hideProgress() {
    Dialog.hideProgress();
  }

  @Override
  public void displayErrorMessage(String message) {
    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void setPresenter(MainActivityContract.Presenter presenter) {

  }

  @Override
  public void initViews() {
    followerList = findViewById(R.id.followerList);
    searchEdit = findViewById(R.id.searchUser);
    searchBtn = findViewById(R.id.search);
    searchBtn.setOnClickListener(this);
    followerList.setLayoutManager(new LinearLayoutManager(this));
  }

  @Override
  public void successResult() {

  }

  @Override
  public void failureResult() {

  }

  @Override
  protected void onResume() {
    super.onResume();
    TransportManager.getInstance().setListener(this);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.search:
        presenter.getSearchText(searchEdit.getText().toString());
        break;
    }
  }

  @Override
  public void onSuccessResponse(int reqType, Object data) {
    presenter.getListFollowers((List<Item>) data);
  }

  @Override
  public void onFailureResponse(int reqType, Object data) {

  }
}
