package dialogutils.blooms.com.testdunjo.service;

import java.util.List;

import dialogutils.blooms.com.testdunjo.model.Item;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IApiService {

    int searchUser = 1;

    @GET("users/{id}/followers")
    Call<List<Item>> search(@Path("id") String appointmidentID);


}
