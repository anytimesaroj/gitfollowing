package dialogutils.blooms.com.testdunjo.DialogUtils;

import android.app.Activity;
import android.app.ProgressDialog;

public class Dialog {
    static ProgressDialog progressBar;

    public static void showProgressBar(Activity activity) {
        progressBar = new ProgressDialog(activity);
        progressBar.setMessage("Loading..");
        progressBar.show();
    }

    public static void hideProgress() {
        if (progressBar != null && progressBar.isShowing())
            progressBar.hide();
    }
}
